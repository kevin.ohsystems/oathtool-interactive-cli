# zsh oathtool interactive cli authcode

Shellscript for 2-factor auth  to get code from user input, output copied to clipboard, using

* zsh
* [oathtool](https://www.nongnu.org/oath-toolkit/index.html)
* [xclip](https://www.nongnu.org/oath-toolkit/index.html)

Thanks to [Stéphane Chazelas](https://unix.stackexchange.com/users/22565/st%c3%a9phane-chazelas) for much of this, via [stackexchange](https://unix.stackexchange.com/questions/686672/echo-checkmark-in-shellscript-zsh-how-to)

![cli recording gif](./assets/clirec.gif "cli recording gif")

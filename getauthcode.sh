#!/bin/sh -
bold_yellow='\033[1;33m'
nc='\033[0m' # no color
if [ "$(locale charmap)" = UTF-8 ]; then
  check_mark='\0342\0234\0224'
else
  check_mark='[X]'
fi
saved_tty_settings=$(stty -g)
trap 'stty "$saved_tty_settings"' EXIT ALRM TERM INT QUIT
stty -echo
printf >&2 "enter your provider's auth code: "
IFS= read -r authcode || exit
secret=$(oathtool --totp -b "$authcode") &&
  printf %s "$secret" | xclip -i &&
  printf '%b\n' "${bold_yellow}${check_mark} code copied to clipboard${nc}"
